import './App.css';

import { foo } from 'components';
import React from 'react';

function App() {
    return (
        <div>{foo}</div>
  );
}

export default App;
